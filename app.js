(function () {
  var express = require('express'),
            _ = require('underscore'),
          url = require('url'),
        async = require('async'),
    Pinterest = require('./pinterest');

  var pinterest = new Pinterest();

  var app = express();
  app.use(express.static(__dirname + '/public'));
  app.get('/', function (req, res) {
    res.sendFile('/public/index.html');
  });

  app.get('/boards/:username', function (req, res) {
    pinterest.getBoards(req.params.username, function (err, boards) {
      res.json(boards);
      res.end();
    });
  });
  
  app.get('/intersection/:user1/:board1/:user2/:board2', function (req, res) {
    async.parallel([
      function (callback) {
        pinterest.getPinsById(req.params.user1, req.params.board1, function (err, pins) {
          callback(null, pins);
        });
      }, 
      function (callback) {
        pinterest.getPinsById(req.params.user2, req.params.board2, function (err, pins) {
          callback(null, pins);
        });
      }], 
      function (err, pins) {
        var results = [];
        var board1 = pins[0];
        var board2 = pins[1];
        var results = [];
        _.each(board1, function (pin1) {
          _.each(board2, function (pin2) {
            if (pin1.sourceUrl === pin2.sourceUrl) {
              results.push({
                sourceUrl: pin1.sourceUrl,
                imageUrl: pin1.imageUrl,
                users: [{
                  username: req.params.user1,
                  url: pin1.url,
                  description: pin1.description
                },
                {
                  username: req.params.user2,
                  url: pin2.url,
                  description: pin2.description
                }]
              });
            }
          })
        })
        res.json(results);
        res.end();
      }
    )
  });

  app.listen(3000);
})();