(function() {
  var async = require('async'),
    cheerio = require('cheerio'),
      https = require('https'),
    request = require('request'),
        url = require('url'),
          _ = require('underscore');

  const PINTEREST_ROOT = 'https://www.pinterest.com';

  var Pinterest = function (config) {

  };

  Pinterest.prototype.getBoards = function (username, callback) {
    request(PINTEREST_ROOT + '/' + username, function (err, res, html) {
      var $ = cheerio.load(html);
      var boardCovers = $('.boardCoverImage');
      var boards = _.map(boardCovers, function (board) {
        var boardUrl = url.parse(PINTEREST_ROOT + $(board).find('.boardLinkWrapper').attr('href'));
        return {
          name: $(board).find('.title').text(),
          url: boardUrl.href,
          owner: boardUrl.path.split("/")[1],
          id: boardUrl.path.split("/")[2]
        };
      });

      callback(null, boards);
    });
  };

  Pinterest.prototype.getPinsById = function (owner, id, callback) {
    getPinsByUrl(PINTEREST_ROOT + '/' + owner + '/' + id, callback);
  };

  Pinterest.prototype.getPins = function (url, callback) {
    getPinsByUrl(url, callback);
  };

  var getPinsByUrl = function (board, callback) {
    request(board, function (err, res, html) {
      var $ = cheerio.load(html);
      var pinWrappers = _.map($('.pinWrapper'), function (data) {
        return data;
      });

      async.map(pinWrappers, function (pin, async_callback) {
        var pinHolder = $(pin).find('.pinHolder');
        var pinLink = $(pinHolder).find('a').attr('href');
        request(PINTEREST_ROOT + pinLink, function (err, res, html) {
          var $ = cheerio.load(html);

          // get source url
          var pinActionBar = $('.PinActionBar');
          var sourceButton = $(pinActionBar).find('a.website');
          var sourceUrl = $(sourceButton).attr('href');

          // get description
          var description = $('.pinDesc.nonCanonicalDesc').text().trim();

          // get image url
          var imageContainer = $('.pinUiImage');
          var imageUrl = $(imageContainer).find('.pinImg').attr('src');

          async_callback(null, {
            url: PINTEREST_ROOT + pinLink,
            description: description,
            sourceUrl: sourceUrl,
            imageUrl: imageUrl
          })
        });
      }, function (err, results) {
        callback(err, results);
      });
    });
  };

  module.exports = Pinterest;
})();