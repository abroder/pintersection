(function () {
  var React = require('react');
  var PintersectionComponent = require('./components/PintersectionComponent');

  React.render(<PintersectionComponent />, document.getElementById('main'));
})();