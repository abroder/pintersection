(function () {
  var React = require('react');
  var $ = require('jquery');

  var PintersectionFormComponent = require('./PintersectionFormComponent');
  var PintersectionListComponent = require('./PintersectionListComponent');

  module.exports = React.createClass({
    getInitialState: function () {
      return {
        user1: {username: "", selectedBoard: ""},
        user2: {username: "", selectedBoard: ""},
        intersection: []
      };
    },

    render: function () {
      var intersectionList;
      if (this.state.intersection.length > 0) {
        intersectionList = <PintersectionListComponent intersection={this.state.intersection} />
      }

      // TODO: avoid passing in the key index if possible
      return (
        <div>
          <PintersectionFormComponent user={this.state.user1} userKey={"user1"} userChanged={this.userChanged} />
          <PintersectionFormComponent user={this.state.user2} userKey={"user2"} userChanged={this.userChanged} />
          <a onClick={this.pintersectClicked} className="btn btn-default">Pintersect!</a>
          {intersectionList}
        </div>
      );
    },

    userChanged: function (user, key) {
      var newState = {};
      newState[key] = user;
      this.setState(newState);
    },

    pintersectClicked: function () {
      var url = '/intersection/' +
        this.state.user1.username + '/' + this.state.user1.selectedBoard + '/' +
        this.state.user2.username + '/' + this.state.user2.selectedBoard;

      $.ajax({url: url, context: this, success: function (res) {
        this.setState({
          intersection: res
        });
      }});
    }
  });
})();