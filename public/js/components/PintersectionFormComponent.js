(function () {
  var React = require('react');
  var $ = require('jquery');

  module.exports = React.createClass({
    getInitialState: function () {
      return {
        boards: []
      };
    },

    render: function () {
      var select;
      if (this.state.boards.length > 0) {
        select = (
          <div className="form-group">
            <select className="form-control" selected={this.props.user.selectedBoard} onChange={this.boardSelected}>
              <option defaultValue disabled hidden value=''></option>
              {this.state.boards.map(function (board) {
                return <option value={board.id}>{board.name}</option>;
              })}
            </select>
          </div>
        );
      }
      return (
        <form className="pintersection-form">
          <div className="form-group">
            <input type="text" className="form-control" value={this.props.user.username} onChange={this.usernameChanged} onBlur={this.usernameBlurred} />
          </div>
          {select}
        </form>
      );
    },

    usernameChanged: function (e) {
      this.props.userChanged({
        username: e.target.value,
        selectedBoard: ""
      }, this.props.userKey);
    },

    usernameBlurred: function () {
      if (this.props.user.username.length === 0) return;

      var url = '/boards/' + this.props.user.username;
      $.ajax({url: url, context: this, success: function (res) {
        this.setState({
          boards: res
        });
      }});
    },

    boardSelected: function (e) {
      this.props.userChanged({
        username: this.props.user.username,
        selectedBoard: e.target.value
      }, this.props.userKey);
    }
  });
})();