(function () {
  var React = require('react');

  module.exports = React.createClass({
    render: function () {
      return (
        <ul id="pintersection-results">
          {this.props.intersection.map(function (elem) {
            return (<li>
              <div className="thumbnail">
                <a href={elem.sourceUrl}>
                  <img src={elem.imageUrl} />
                </a>
                <div className="caption">
                  <p> 
                    {elem.users[0].username} says "{elem.users[0].description}"
                  </p>
                  <p> 
                    {elem.users[1].username} says "{elem.users[1].description}"
                  </p>
                </div>
              </div>
            </li>);
          })}
        </ul>
      );
    }
  });
})();